var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var Comment = new Schema({
	content: String,
	creater: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
	referencePost: {
    type: Schema.Types.ObjectId,
    ref: 'Post'
  },
  createdDate: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model('Comment', Comment);
