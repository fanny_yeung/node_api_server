var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var Post = new Schema({
	creater: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  createdDate: {
    type: Date,
    default: new Date()
  },
  name: String,
	description: {
		description: String,
		condition: String
	},
	price: Number,
	photo: String,
	category: String,
	like: [{
		type: Schema.Types.ObjectId,
		ref: 'User'
	}]
});

module.exports = mongoose.model('Post', Post);
