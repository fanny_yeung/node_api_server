var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var user = new Schema({
	userName: String,
	email: String,
  joinDate: {
    type: Date,
    default: new Date()
  },
  phoneNumber: [String],
	profilepic: String
	follower: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
	following: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }]

});

module.exports = mongoose.model('user', user);
