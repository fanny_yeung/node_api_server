var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var utility = require('./utility');

var routes = require('./routes/index');
var userRoute = require('./routes/userRoute');
var postRoute = require('./routes/postRoute');
var commentRoute = require('./routes/commentRoute');
// var messageRoute = require('./routes/messageRoute');

var app = express();

// Connection to MongoDB
var mongoose = require('mongoose');
mongoose.connect(utility.config.mongodb.host, utility.config.mongodb.options);
var db = mongoose.connection;
db.on('error', function(err) {console.log(err)});
db.once('open', function() {console.log("### Connected to database!")});

// MongoDB Admin UI
var mongo_express = require('mongo-express/lib/middleware');
var mongo_express_config = require('./mongo_express_config');
app.use('/mongo_express', mongo_express(mongo_express_config));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/user', userRoute);
app.use('/post', postRoute);
// app.use('/message', messageRoute);
app.use('/comment', commentRoute);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
