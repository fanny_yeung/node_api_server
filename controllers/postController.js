var Post = require('../models/Post');

module.exports = {
  createPost: function(req, res) {
    var post = new Post(req.body);
    post.save( function(err, post) {
      if (err) console.log(err);
      if (post) res.json({Success: post});
      if (!post) res.json({Error: "Cannot create model with input parameters"});
    });
  },

  list: function (req, res) {
    Post.find(function (err, Posts) {
      if (err) {
        return res.status(500).json({
          message: 'Error when getting Post.',
          error: err
        });
      }
      return res.json(Posts);
    });
  },

  viewOne: function(req, res) {
    Post.findOne({_id: req.params.id})
    .exec((err, postModel) => {
      res.json(postModel);
    });
  },

  updatePost: function(req, res){
    Post.findById(req.body.postId, function(err, e){
      e.price = req.body.price;

      e.save(function(err, updatedPost){
        if(err) console.log(err);
        if(updatedPost) res.json({success: updatedPost});
      });
    });
  },

  delete: function(req, res){
    Post.findById(req.body.postId, function(err, e){
      if(err) return handleError(err);

      e.remove(function(err, result){
        if(err) console.log(err);
        if(result) res.json({success: "Deleted"});
      });
    });
  }


}
