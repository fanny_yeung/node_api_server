var Comment = require('../models/Comment');

module.exports = {
  createComment: function(req, res) {
    var comment = new Comment(req.body);
    comment.save( function(err, comment) {
      if (err) console.log(err);
      if (comment) res.json({Success: comment});
      if (!comment) res.json({Error: "Cannot create model with input parameters"});
    });
  },

  list: function (req, res) {
          comment.find(function (err, Comments) {
              if (err) {
                  return res.status(500).json({
                      message: 'Error when getting Comment.',
                      error: err
                  });
              }
              return res.json(Comments);
          });
      }


}
