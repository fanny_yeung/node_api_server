var User = require('../models/User');

module.exports = {
  createUser: function(req, res) {
    var user = new User(req.body);
    user.save( function(err, user) {
      if (err) console.log(err);
      if (user) res.json({Success: user});
      if (!user) res.json({Error: "Cannot create model with input parameters"});
    });
  },

  list: function (req, res) {
    User.find(function (err, Users) {
      if (err) {
        return res.status(500).json({
          message: 'Error when getting User.',
          error: err
        });
      }
      return res.json(Users);
    });
  },

  viewOne: function(req, res) {
    User.findOne({_id: req.params.id})
    .exec((err, userModel) => {
      res.json(userModel);
    });
  },

  updateUser: function(req, res){
    User.findById(req.body.userId, function(err, e){
      e.userName = req.body.userName;

      e.save(function(err, updatedUser){
        if(err) console.log(err);
        if(updatedUser) res.json({success: updatedUser});
      });
    });
  },

  delete: function(req, res){
    User.findById(req.body.userId, function(err, e){
      if(err) return handleError(err);

      e.remove(function(err, result){
        if(err) console.log(err);
        if(result) res.json({success: "Deleted"});
      });
    });
  }

}
