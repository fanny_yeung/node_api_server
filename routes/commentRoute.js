var express = require('express');
var router = express.Router();
var commentController = require('../controllers/commentController');

/* GET home page. */

/* Post a post to db*/
router.post('/', commentController.createComment);

/* GET whole list of post */
router.get('/', commentController.list);

module.exports = router;
