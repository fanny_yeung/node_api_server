var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController');

/* Post a post to db*/
router.post('/', userController.createUser);
router.post('/update', userController.updateUser);
router.post('/delete', userController.delete);

/* GET whole list of post */
router.get('/', userController.list);
router.get('/viewOne/:id', userController.viewOne);
module.exports = router;
