var express = require('express');
var router = express.Router();
var postController = require('../controllers/postController');

/* GET home page. */

/* Post a post to db*/
router.post('/', postController.createPost);
router.post('/update', postController.updatePost);
router.post('/delete', postController.delete);



/* GET whole list of post */
router.get('/', postController.list);
router.get('/viewOne/:id', postController.viewOne);

module.exports = router;
